
public class bubbleSort {

	public static void main(String[] args) {
		int[] a = { 12, 5, 3, 32, 8 };
		bubble_sort(a);
		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
	}
	public static int[] bubble_sort(int[] a) {
		for (int i = 0; i < a.length - 1; i++) {
			for (int j = 0; j < a.length - i - 1; j++) {
				if (a[j] > a[j + 1]) {
					swap(a, j);
				}
			}
		}
		return a;
	}
	public static void swap(int a[], int j) {
		int temp = a[j];
		a[j] = a[j + 1];
		a[j + 1] = temp;
	}

}