public class Topic {
	static int ID;
	static String name;
	static String rfid;

	public static int getID() {
		return ID;
	}

	public static void setID(int iD) {
		ID = iD;
	}

	public static String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static String getRfid() {
		return rfid;
	}

	public void setRfid(String rfid) {
		this.rfid = rfid;
	}

	public Topic(int iD, String name, String rfid) {
		super();
		ID = iD;
		this.name = name;
		this.rfid = rfid;
	}

}
