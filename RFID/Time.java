import java.sql.Date;

public class Time {
	static int ID;
	static Date startTime;
	static Date endTime;
	static int topicID;

	public static int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public static Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public static Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public static int getTopicID() {
		return topicID;
	}

	public void setTopicID(int topicID) {
		this.topicID = topicID;
	}

	public Time(int iD, Date startTime, Date endTime, int topicID) {
		super();
		ID = iD;
		this.startTime = startTime;
		this.endTime = endTime;
		this.topicID = topicID;
	}

}
