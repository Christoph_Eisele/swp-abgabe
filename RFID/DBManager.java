import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DBManager {
	public static void tableTopic() throws SQLException {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/SWP_RFID.db");
			stmt = c.createStatement();
			String sql = "CREATE TABLE IF NOT Exists Topic" + "(ID int PRIMARY KEY NOT NULL,"
					+ "name varchar(100) NOT NULL, " + "RFID varchar(100) NOT NULL);";
			stmt.executeUpdate(sql);
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	public static void tableTime() throws SQLException {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/SWP_RFID.db");
			stmt = c.createStatement();
			String sql = "CREATE TABLE IF NOT Exists Time" + "(ID int PRIMARY KEY NOT NULL,"
					+ "startTime DateTime NOT NULL, " + "endTime DateTime NOT NULL," + "topicID int NOT NULL,"
					+ "Foreign Key(topicID) References Topic(ID) ON DELETE Cascade);";
			stmt.executeUpdate(sql);
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	public static void TopicAnlegen() throws SQLException {
		Connection c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/SWP_RFID.db");
			String sql = "INSERT INTO Topic Values(?,?,?);";
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setInt(0, Topic.getID());
			stmt.setString(1, Topic.getName());
			stmt.setString(2, Topic.getRfid());
			stmt.executeUpdate();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}

	public static void TimeAnlegen() throws SQLException {
		Connection c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/SWP_RFID.db");
			String sql = "INSERT INTO Time Values(?,?,?,?);";
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setInt(1, Time.getID());
			stmt.setDate(2, Time.getStartTime());
			stmt.setDate(3, Time.getEndTime());
			stmt.setInt(4, Time.getTopicID());
			stmt.executeUpdate();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}

	public static void update(String tabelle, int ID, String spalte, Date neuerWert) throws SQLException {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/SWP_RFID.db");
			stmt = c.createStatement();
			String sql = "UPDATE" + tabelle + "SET" + spalte + "=" + neuerWert + "WHERE ID =" + ID;
			stmt.executeQuery(sql);

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}

	public ArrayList<Topic> holeTopic() throws SQLException {
		Connection c = null;
		ArrayList<Topic> m = new ArrayList<Topic>();
		String sql = "SELECT * FROM Topic";
		Statement stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		while (rs.next()) {
			int ID = rs.getInt("ID");
			String name = rs.getString("name");
			String rfid = rs.getString("rfid");
			m.add((new Topic(ID, name, rfid)));
		}
		rs.close();
		stmt.close();
		return m;
	}

	public ArrayList<Time> holeTime() throws SQLException {
		Connection c = null;
		ArrayList<Time> m = new ArrayList<Time>();
		String sql = "SELECT * FROM Time";
		Statement stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		while (rs.next()) {
			int ID = rs.getInt("ID");
			Date startTime = rs.getDate("startTime");
			Date endTime = rs.getDate("endTime");
			int topicID = rs.getInt("topicID");
			m.add((new Time(ID, startTime, endTime, topicID)));
		}
		rs.close();
		stmt.close();
		return m;
	}

	public void delete(String tabelle, int ID) throws SQLException {
		Connection c = null;
		c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/SWP_RFID.db");
		String sql = "DELETE FROM ? WHERE ID = ? ";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, tabelle);
		stmt.setInt(2, ID);
		stmt.executeUpdate();
		stmt.close();
		c.close();
	}
}
