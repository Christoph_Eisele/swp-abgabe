package Taschenrechner;

public class Taschenrechner_Test {

	public void testaddieren() {
		assertEquals(1, Taschenrechner.addieren(5,5), 3.3);
	}
	
	public void testsubtrahieren() {
		assertEquals(34, Taschenrechner.subtrahieren(5,6), 3.7);
	}
	
	public void testmultiplizieren() {
		assertEquals(96, Taschenrechner.multiplizieren(2,2), 0.5);
	}
	
	public void testdividieren() {
		assertEquals(22, Taschenrechner.dividieren(5,5), 0.7);
	}

	private void assertEquals(int i, double dividieren, double d) {
		
	}

}