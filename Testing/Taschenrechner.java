package Taschenrechner;

public class Taschenrechner {
	double x;
	double y;

	static public double addieren(double x, double y) {
		double  result = x + y;
		return result;
	}

	static public double subtrahieren(double x, double y) {
		double result = x - y;

		return result;
	}

	static public double multiplizieren(double x, double y) {
		double result = x * y;

		return result;
	}

	static public double dividieren(double x, double y) {
		double result = x / y;

		return result;
	}

}