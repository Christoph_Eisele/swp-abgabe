
public class Transporter extends Fahrzeug{

	private int sitzplaetze;

	public Transporter(int sitzplaetze) {
		super();
		this.sitzplaetze = sitzplaetze;
	}

	public int getSitzplaetze() {
		return sitzplaetze;
	}
	
	public void setSitzplaetze(int sitzplaetze) {
		this.sitzplaetze = sitzplaetze;
	}
	
	
	
}
