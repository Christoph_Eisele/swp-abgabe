
public class Motorrad extends Fahrzeug{
	private int hubraum;

	public int getHubraum() {
		return hubraum;
	}

	public void setHubraum(int hubraum) {
		this.hubraum = hubraum;
	}

	public Motorrad(int hubraum) {
		super();
		this.hubraum = hubraum;
	} 	
	
}
