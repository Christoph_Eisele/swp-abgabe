
public class PKW extends Fahrzeug{
    private String besonderheit;

	public String getBesonderheit() {
		return besonderheit;
	}

	public void setBesonderheit(String besonderheit) {
		this.besonderheit = besonderheit;
	}

	public PKW(String besonderheit) {
		super();
		this.besonderheit = besonderheit;
	} 
}
