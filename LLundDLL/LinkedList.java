public class LinkedList {

	public class Node {
		Node next;
		int value;

		public Node(Node next, int value) {
			this.next = next;
			this.value = value;
		}

		public Node getNext() {
			return next;
		}

		public void setNext(Node next) {
			this.next = next;
		}

		public int getValue() {
			return value;
		}

		public void setValue(int value) {
			this.value = value;
		}

	}

	Node head = null;

	public LinkedList() {
		addElement(3);
		addElement(7);
		addElement(4);
		Node n = head;
		while (n.next != null) {
			System.out.println(n.getValue());
			n = n.getNext();
		}
		System.out.println(n.getValue());
	}

	public void addElement(int Value) {

		if (head == null) {
			head = new Node(null, Value);
		} else {
			Node n = head;
			while (n.next != null) {
				n = n.getNext();
			}
			n.setNext(new Node(null, Value));
		}
	}

	public static void main(String[] args) {
		new LinkedList();
	}

}