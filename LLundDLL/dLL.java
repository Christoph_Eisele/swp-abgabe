public class dLL {

	int size = 0;
	Node head = null;
	Node tail = null;

	class Node {
		int value;
		Node next;
		Node previous;

		public Node(int value) {
			this.value = value;
			next = null;
			previous = null;
		}
	}

	public Node addFirst(int value) {
		Node n = new Node(value);
		if (size == 0) {
			head = n;
			tail = n;
		} else {
			n.next = head;
			head.previous = n;
			head = n;
		}
		size++;
		return n;
	}

	public Node addLast(int value) {
		Node n = new Node(value);
		if (size == 0) {
			head = n;
			tail = n;
		} else {
			tail.next = n;
			n.previous = tail;
			tail = n;
		}
		size++;
		return n;
	}

	public void print() {
		Node n = head;
		while (n != null) {
			System.out.print(" " + n.value);
			n = n.next;
		}
		System.out.println();
	}

	public static void main(String[] args) {
		dLL d = new dLL();
		d.addLast(3);
		d.addLast(2);
		d.addFirst(3);
		d.addFirst(1);
		d.print();
	}	
}
